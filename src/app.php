<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/cloud.php';

/*
 * A simple Slim based sample application
 *
 * See Slim documentation:
 * http://www.slimframework.com/docs/
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Views\PhpRenderer;
use \LeanCloud\Client;
use \LeanCloud\Storage\CookieStorage;
use \LeanCloud\Engine\SlimEngine;
use \LeanCloud\Query;
use \LeanCloud\LeanObject;

$app = new \Slim\App();
// 禁用 Slim 默认的 handler，使得错误栈被日志捕捉
unset($app->getContainer()['errorHandler']);

Client::initialize(
    getenv("LEANCLOUD_APP_ID"),
    getenv("LEANCLOUD_APP_KEY"),
    getenv("LEANCLOUD_APP_MASTER_KEY")
);
// 将 sessionToken 持久化到 cookie 中，以支持多实例共享会话
Client::setStorage(new CookieStorage());
Client::useProduction((getenv("LEANCLOUD_APP_ENV") === "production") ? true : false);

//SlimEngine::enableHttpsRedirect();
$app->add(new SlimEngine());

// 使用 Slim/PHP-View 作为模版引擎
$container = $app->getContainer();
$container["view"] = function ($container) {
    return new \Slim\Views\PhpRenderer(__DIR__ . "/views/");
};

$app->get('/', function (Request $request, Response $response) {
    $xxid = (int)isset($_GET['xxid']) ? intval($_GET['xxid']) : 1990;
    $ipPool = [
        '47.112.114.31', //0 10线
        '47.112.123.212', //1 12线
        '47.112.124.15', //2 13线
        '47.112.113.220', //3 14线
        '47.112.126.185', //4 15线
        '47.107.247.25', //5 16线
        '47.112.106.253', //6 17线
        '47.112.123.225', //7 18线
        '47.112.124.224', //8 19线
        '47.107.127.27', //9 20线
        '47.112.125.196', //10 21线
        '47.112.124.38', //11 22线
        '47.112.109.37', //12 23线
        '47.112.127.107', //13 24线
        '47.112.117.221', //14 25线
        '47.107.124.152', //15 26线
        '47.112.125.162', //16 27线
        '47.112.124.69', //17 28线
        '47.112.126.42', //18 29线
		'47.107.252.91', //19 30线
		'47.112.106.178', //20 31线
		'47.112.114.141', //21 32线
		'47.112.126.5', //22 33线
		'47.112.111.179', //23 34线
        '47.107.228.216', //24 35线
        '47.112.127.171', //25 36线
        '47.112.122.196', //26 37线
        '47.112.96.107', //27 38线
        '47.112.110.171', //28 39线
        '47.112.109.252', //29 40线

        '47.107.255.86', //30 41线
        '47.112.121.143', //31 42线
        '47.112.113.132', //32 43线
        '47.107.236.234', //33 44线
        '47.107.240.165', //34 45线
        '47.107.243.30', //35 46线

        '47.112.96.131', //36 47线
        '47.107.235.66', //37 48线
        '47.112.116.201', //38 49线
        '47.112.100.50', //39 50线
        '47.107.234.0', //40 51线
        '47.112.127.188', //41 52线

        '47.107.254.110', //42 53线
        '47.112.112.25', //43 54线
        '47.112.117.131', //44 55线
        '47.112.105.75', //45 56线
        '47.112.114.18', //46 57线
        '47.112.127.217', //47 58线
    ];
    if (array_key_exists($xxid, $ipPool)) {
        $data = curlGet("http://{$ipPool[$xxid]}/getData.php");
        return $this->view->render($response, "jump2.phtml", array(
            "currentTime" => new \DateTime(),
            'data' => json_decode($data, true)
        ));
    } elseif(isset($_GET['path'])) {
         return $this->view->render($response, "jump2.phtml", array(
            "currentTime" => new \DateTime(),
            'data' => []
        ));
    }else {
        header("Location: http://www.baidu.com");
    }
});

$app->get('/alex/{id}[/{params:.*}]', function (Request $request, Response $response, $args) {
    $data = curlGet('http://120.78.80.160');
//    var_dump($args);die;
    return $this->view->render($response, "result.phtml", array(
        "currentTime" => new \DateTime(),
        'data' => json_decode($data, true),
        'id' => $args['id']
    ));
});

// 显示 todo 列表
$app->get('/todos', function (Request $request, Response $response) {
    $query = new Query("Todo");
    $query->descend("createdAt");
    try {
        $todos = $query->find();
    } catch (\Exception $ex) {
        error_log("Query todo failed!");
        $todos = array();
    }
    return $this->view->render($response, "todos.phtml", array(
        "title" => "TODO 列表",
        "todos" => $todos,
    ));
});

$app->post("/todos", function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $todo = new LeanObject("Todo");
    $todo->set("content", $data["content"]);
    $todo->save();
    return $response->withStatus(302)->withHeader("Location", "/todos");
});

$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});

$app->run();

